function Synchronization(param) {

    // var osname = Ti.Platform.osname;

    // var syncUrl = "http://192.168.20.245/01.FONTE/eMob_product/modules/eMob_Sync_PS/webservice.php";
    // var syncUrl = "http://gowhpt01/HyperTrade/modules/eMob_Sync_PS/webservice.php";
    var Compression = require('ti.compression');

    var syncError = false;
    var errorStr = '';

    Ti.include('ui/common/include/CryptLib.js');
    var cryptFunct = new CryptLib({});

    Ti.include('/ui/common/include/Database.js');
    dbOps = new Database({});

    Ti.include('/ui/common/include/TypeConvert.js');
    var tc = new TypeConvert({});

    var syncUrl = dbOps.select("SELECT sync_url FROM b_config WHERE config='url'")[0].sync_url;
    var dbrows = dbOps.select('SELECT data, data2 FROM b_config WHERE config = \'firstInLogin\'');
    Ti.API.info("dbrows = " + JSON.stringify(dbrows));

    var syncronizationId = guid();

    var auth = 'Basic ' + Ti.Utils.base64encode(cryptFunct.decryptStr(dbrows[0].data) + ':' + cryptFunct.decryptStr(dbrows[0].data2));
    Ti.API.info('Credentials: ' + dbrows[0].data + ':' + dbrows[0].data2);
    Ti.API.info(auth);

    var LoadingView = require('/ui/common/include/LoadingView');
    var loading = new LoadingView({
        message: L('sync_loading', '  Sincronizando...  '),
        steps: 6
    });

    loading.win.addEventListener('androidback', function () {
        Ti.API.info("Loading back");
    });

    var attData = []; // Vetor que armazena informações dos anexos a serem baixados do servidor
    var attDataUniq; // Vetor que armazena apenas informações únicas dos anexos
    var attElem = {}; // Objeto auxiliar que guarda informações do anexo que será baixado
    var visitaIdArray; // Vetor que armazena os Ids das visitas para buscar atividades
    var max_pages; // Número total de páginas de download de dados
    var page = 1; // Página a ser pedida no download de dados

    GetWS = function (method, params, callback, sync_id) {
        var str = JSON.stringify({
            method: method,
            params: params,
            sync_id: sync_id
        });
        var xhr = Titanium.Network.createHTTPClient();
        Ti.API.info('########### GetWS');
        xhr.onload = function () {
            Ti.API.info('Method: ' + method + ' Params: ' + JSON.stringify(params) + ' responseText ' + this.responseText);
            Ti.API.info('str: ' + str);
            if (typeof callback == 'function') {
                try {
                    callback(JSON.parse(this.responseText));
                } catch (e) {
                    loading.close();
                    Log.info("Erro1 : "+e)
                    var alertCustom = util.createAlertCustom({
                        message : "sync_error_request"
                        // message : "Houve um erro na sincronização de dados. Por favor tente novamente mais tarde."
                    });

                    alertCustom.open();
                    // alert("Houve um erro na sincronização de dados. Por favor tente novamente mais tarde.");
                }
            } else {
                // closeWindow(loading);
                loading.close();
            }
        };
        xhr.onerror = function (e) {
            syncError = true;
            Ti.API.info("Erro em GetWS - method: " + method + " params: " + params + " erro: " + e.error);
            util.logError('Erro de requisição em GetWS', e.error);
            loading.close();
            Log.info("Erro2 : "+e)
            var alertCustom = util.createAlertCustom({
                message : "no_internet"
                // message : "Houve um erro na sincronização de dados. Por favor tente novamente mais tarde."
            });

            alertCustom.open();
            // alert("Houve um erro na sincronização de dados. Por favor tente novamente mais tarde.");
            // closeWindow(loading);
        };
        xhr.timeout = 300000;
        xhr.open("GET", syncUrl + '?json=' + str);
        xhr.setRequestHeader('Authorization', auth);
        xhr.send();
        Ti.API.info("Requisição GET enviada");
    };
    PutWS = function (method, params, callback, customBlob, sync_id) {
        var str = JSON.stringify({
            method: method,
            params: params,
            sync_id: sync_id
        });

        str = str.replace('"base64":{}', customBlob);

        var xhr = Titanium.Network.createHTTPClient();
        Ti.API.info('########### PutWS');
        xhr.onload = function () {
            Ti.API.info('Method: ' + method + ' Params: ' + JSON.stringify(params) + ' responseText ' + this.responseText);
            if (typeof callback == 'function') {
                callback(JSON.parse(this.responseText));
            } else {
                // closeWindow(loading);
            }
        };
        xhr.onerror = function (er) {
            Ti.API.info('Error! PutWS');
            Ti.API.info("Erro em PutWS - method: " + method + " params: " + JSON.stringify(params) + " erro: " + er.error);
            // closeWindow(loading);
            syncError = true;
            util.logError('Erro de requisição em PutWS', er.error);
        };
        xhr.open("PUT", syncUrl);
        xhr.setRequestHeader('Authorization', auth);
        // xhr.send('json=' + str);
        xhr.send(body);
    };
    PostWS = function (method, params, callback, sync_id) {
        var str = JSON.stringify({
            method: method,
            params: params,
            sync_id: sync_id
        });

        var body = {
            json: {
                method: method,
                params: params,
                sync_id: sync_id
            }
        };

        var xhr = Titanium.Network.createHTTPClient();
        Ti.API.info('########### PostWS');
        xhr.onload = function () {
            Ti.API.info('Response Text ' + this.responseText);
            Ti.API.info('str: ' + str);
            if (typeof callback == 'function') {
                try {
                    callback(JSON.parse(this.responseText));
                } catch (e) {
                    loading.close();
                    var alertCustom = util.createAlertCustom({
                        message : "sync_error_request"
                        // message : "Houve um erro na sincronização de dados. Por favor tente novamente mais tarde."
                    });

                    alertCustom.open();
                    // alert("Houve um erro na sincronização de dados. Por favor tente novamente mais tarde.");
                }
            } else {
                // closeWindow(loading);
                loading.close();
                util.createAlertCustom(this.responseText);
            }
        };
        xhr.onerror = function (e) {
            syncError = true;
            Ti.API.info("Erro em PostWS - method: " + method + " params: " + params + " erro: " + e.error);
            util.logError('Erro de requisição em PostWS', e.error);
            loading.close();
            var alertCustom = util.createAlertCustom({
                message : "no_internet"
                // message : "Houve um erro na sincronização de dados. Por favor tente novamente mais tarde."
            });

            alertCustom.open();
            // closeWindow(loading);
        };
        xhr.open("POST", syncUrl);
        xhr.setRequestHeader('Authorization', auth);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');
        xhr.send('json=' + str);
        // xhr.send(JSON.stringify(body));
    };

    SyncSQL = function () {
        Ti.API.info('######### SyncSQL');

        // loading.increment('Download de updates');
        loading.increment('sync_download_update');
        loading. setProgressProps({
            detail: 'sync_waiting_device'
            // detail: 'Aguardando Dipositivo'
        })

        var appVersion = Ti.App.version;
        var osVersion = Ti.Platform.version;
        var model = Ti.Platform.model;
        // var pdv_number = dbOps.select("SELECT COUNT(1) AS pdv_number FROM b_org_ext WHERE deleted_by = ''")[0].pdv_number;
        // var datetime = dbOps.select("SELECT DATETIME('NOW', 'LOCALTIME') AS datetime")[0].datetime;
        var datetime = dbOps.select("SELECT DATETIME('NOW') AS datetime")[0].datetime;

        var method = 'SQLUpdate';
        var params = {
            appVersion: appVersion,
            osVersion: osVersion,
            model: model,
            // pdv_number: pdv_number,
            deviceDatetime: datetime
        };
        GetWS(method, params, SyncSQLCallback, syncronizationId);
    };
    SyncSQLCallback = function (data) {
        Ti.API.info('######### SyncSQLCallback');

        loading.increment('sync_exec_updates');
        // loading.increment('Executando updates');
        loading.setProgressProps({
            detail: 'sync_waiting_device'
            // detail: 'Aguardando dispositivo'
        });

        if (data['return'] == 'error') {
            util.logError('Erro no callback de SyncSQL', data['msg']);
            Ti.API.info('Erro: ' + data['msg']);
            loading.close();
            util.createAlertCustom(data['msg']);
            // closeWindow(loading);
            return false;
        }

        for (var i in data) {
            execQuery(data[i]);
        }

        if ( !! syncError) {
            // closeWindow(loading);
            loading.close();
            var alertCustom = util.createAlertCustom({
                message : L('sync_error_adm', 'Houve um erro na sincronização. Contacte o administrador do sistema. ') + 'SyncSQL.',
                msgIsFormat : true
                // message : "Houve um erro na sincronização. Contacte o administrador do sistema. SyncSQL"
            });

            alertCustom.open();
            // alert('Houve um erro na sincronização. Contacte o administrador do sistema. SyncSQL');
            return false;
        }

        loading.increment('sync_data_download');
        // loading.increment('Download de dados');
        loading.setProgressProps({
            detail: 'sync_waiting_server'
            // detail: 'Aguardando servidor'
        });
        SyncDownload();
    };

    SyncUpload = function () {
        Ti.API.info('######### SyncUpload');

        loading.increment('sync_upload');
        // loading.increment('Enviando registros');
        loading.setProgressProps({
            detail: 'sync_waiting_device'
            // detail: 'Aguardando dispositivo'
        });

        var method = 'UploadSync';
        var queries = {};
        var savedQueries = dbOps.select("SELECT rowid_, statement FROM t_sync_statement ORDER BY last_upd ASC");
        for (var i = 0; i < savedQueries.length; i++) {

            // Lógica para retirar '<' e '>' da query
            var isInsert = (savedQueries[i].statement.toLowerCase().indexOf('insert' === 0)) ? true : false;
            var queryFirstPart = savedQueries[i].statement.match(/.*(?=where)/i);
            if (isInsert === false && queryFirstPart != null) {
                var querySecondPart = savedQueries[i].statement.substring(queryFirstPart[0].length, savedQueries[i].statement.length);
                queryFirstPart[0] = queryFirstPart[0].replace(/<|>/gm, '');
                savedQueries[i].statement = queryFirstPart[0] + querySecondPart;
            } else {
                savedQueries[i].statement = savedQueries[i].statement.replace(/<|>/gm, '');
            }

            queries[savedQueries[i].rowid_] = encodeURIComponent((savedQueries[i].statement).replace(/\"/g, "\\\""));
        }

        // PutWS(method, queries, SyncUploadCallback, null, syncronizationId);
        PostWS(method, queries, SyncUploadCallback, syncronizationId);
    };

    SyncUploadCallback = function (data) {
        Ti.API.info('######### SyncUploadCallback');
        if (data['return'] == 'error') {
            loading.close();
            // closeWindow(loading);
            util.logError('Erro no callback de SyncUpload', data['msg']);
            util.createAlertCustom(data['msg']);
            return false;
        }
        if (data['return'] == 'success') {
            dbOps.del({
                query: "DELETE FROM t_sync_statement"
            });
        } else {
            loading.close();
            // closeWindow(loading);
            Ti.API.info("data: " + JSON.stringify(data));
            var alertCustom = util.createAlertCustom({
                message : L('sync_error_adm', 'Houve um erro na sincronização. Contacte o administrador do sistema. ') + 'SyncUpload.',
                msgIsFormat : true
                // message : "Houve um erro na sincronização. Contacte o administrador do sistema. SyncUpload"
            });

            alertCustom.open();
            // alert('Houve um erro na sincronização. Contacte o administrador do sistema. SyncUpload');
            syncError = true;
            return false;
        }

        SyncAttachments();
    };

    SyncAttachments = function () {

        Ti.API.info('######### SyncAttachments');
        var method = 'attachmentSync';
        var queries = {};
        var statement = "SELECT rowid_, url FROM b_attachment WHERE created > (select last_upd from b_config where config='firstInLogin')";
        var retorno = [];
        var encodeData = false;

        var attachments = dbOps.select(statement);
        Ti.API.info('attachments length = ' + attachments.length);

        for (var i = 0; i < attachments.length; i++) {
            Ti.API.info('Buscando anexo ' + i);
            var registro = {};
            var file = null;

            if (osname == 'android') {
                if (Ti.Filesystem.isExternalStoragePresent) {
                    var fileDir = Ti.Filesystem.getFile(Ti.Filesystem.externalStorageDirectory, '.camera');
                    Ti.API.info('Existe sdcard');
                    if (fileDir.exists()) {
                        file = Ti.Filesystem.getFile(fileDir.nativePath, attachments[i].url);
                        Ti.API.info('Existe diretório .camera');
                    } else {
                        file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, attachments[i].url);
                        Ti.API.info('Não existe diretório .camera');
                    }
                } else {
                    file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, attachments[i].url);
                    Ti.API.info('Não existe sdcard');
                }
            } else {
                file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, attachments[i].url);
            }

            // var f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, attachments[i].url);

            if (file.exists()) {
                Ti.API.info('Encontrou arquivo');
            } else {
                Ti.API.info('Não encontrou arquivo');
            }

            Ti.API.info('File Size = ' + file.size);
            var temp = file.read();

            encodeData = Ti.Utils.base64encode(temp);

            registro['id'] = attachments[i].rowid_;
            registro['path'] = attachments[i].url;
            // registro['titulo'] = attachments[i].name;
            if (osname == 'android') {
                registro['base64'] = encodeData;
            } else {
                registro['base64'] = {};
                registro['base64']['text'] = encodeData.toString().replace(/(\r\n|\n|\r)/gm, "");
            }

            Ti.API.info('rowid_ = ' + attachments[i].rowid_);
            Ti.API.info('url = ' + attachments[i].url);
            Ti.API.info('base64 = ' + encodeData);

            // registro['base64'] = customBlob;

            // var str1 = '"base64":{"width":0,"length":' +
            //  file.size +
            //  ',"height":0,"bubbleParent":true,"mimeType":"text\/plain","text":"' +
            //  encodeData.toString() +
            //  '","type":3,"nativePath":"","file":""}';
            // str1 = str1.replace(/\n/gm, '');
            // str1 = str1.replace(/\r/gm, '');

            retorno.push(registro);
        }

        // PutWS(method, retorno, SyncAttachmentsCallback, str1, syncronizationId);
        PostWS(method, retorno, SyncAttachmentsCallback, syncronizationId);
    };

    SyncAttachmentsCallback = function (data) {
        Ti.API.info('######### SyncAttachmentsCallback');
        if (data['return'] == 'error') {
            loading.close();
            // closeWindow(loading);
            util.logError('Erro no callback de SyncAttachment', data['msg']);
            util.createAlertCustom(data['msg']);
            return false;
        }

        if (data['return'] == 'success') {

        } else {
            loading.close();
            // closeWindow(loading);
            var alertCustom = util.createAlertCustom({
                message : L('sync_error_adm', 'Houve um erro na sincronização. Contacte o administrador do sistema. ') + 'SyncAttachmentsCallback.',
                msgIsFormat : true
            });

            alertCustom.open();
            // alert('SyncAttachmentsCallback - Houve um erro na sincronização. Contacte o administrador do sistema.');
            syncError = true;
            return false;
        }

        // SyncDownload();
        dbOps.update({
            query: "update b_config set last_upd='" + tc.getCurrentTimeInStorageFormat() + "'",
            dontSaveForSync: true
        });

        // deleteData();
        loading.close();

        if (param.onSuccess)
            param.onSuccess();
        var alertCustom = util.createAlertCustom({
            message : 'sync_success'
            // message : "A sincronização foi finalizada com sucesso."
        });

        alertCustom.open();

        // alert('A sincronização foi finalizada com sucesso.');

        // Ti.App.fireEvent('redrawMenu'); // Atualiza o menu
    };

    SyncDownload = function (page) {
        Ti.API.info('######### SyncDownload');

        var method = 'DownloadSync';
        var params;

        if (typeof page != "undefined") {
            Ti.API.info("Pedindo página " + page + " de " + max_pages);
            params = {
                page: page
            };
        } else {
            params = {};
        }

        GetWS(method, params, SyncDownloadCallback, syncronizationId);
    };

    SyncDownloadCallback = function (data) {
        Ti.API.info('######### SyncDownloadCallback');

        // loading.increment('Inserindo dados no banco');
        // loading.setProgressProps({
        //  detail: 'Aguardando dispositivo'
        // });

        if (typeof max_pages == 'undefined') {
            loading.setProgressProps({
                detail: 'sync_first_page'
                // detail: 'Processando página 1'
            });
        } else {
            var detailMsg = String.format(L('sync_current_page'), page, max_pages);
            if (page <= max_pages) {
                loading.setProgressProps({
                    detail: detailMsg,
                    msgIsFormat : true
                    // detail: 'Processando página ' + page + " de " + max_pages
                });
            }
        }

        if (data['return'] == 'error') {
            loading.close();
            // closeWindow(loading);
            util.logError('Erro no callback de SyncDownload', data['msg']);
            util.createAlertCustom(data['msg']);
            return false;
        }

        var b64Data = data["zip"];

        // Salva o arquivo zip da página
        try {
            var fileDir;
            if (osname == 'android')
                fileDir = Ti.Filesystem.getFile(Ti.Filesystem.externalStorageDirectory, '.emob'); // Cria uma pasta oculta para evitar que usuário apague
            else
                fileDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory);

            if (!fileDir.exists()) {
                fileDir.createDirectory();
            }

            // .resolve() provides the resolved native path for the directory.
            var zippedFile = Ti.Filesystem.getFile(fileDir.nativePath, "zippedPage.zip");
            Log.info("zippedFile path is: " + zippedFile.resolve());
            if (zippedFile.write(Ti.Utils.base64decode(b64Data)) === false && osname == 'android') {
                logError('Erro na criação de anexo', 'Não foi possível criar o anexo - Write Error');
                Log.error('Não foi possível criar a foto - Write Error');
            } else if ((osname == 'iphone' || osname == 'ipad') && !zippedFile.isFile()) {
                logError('Erro na criação de anexo', 'Não foi possível criar o anexo - Not a File');
                Log.error('Não foi possível criar a foto - Not a File');
            } else {
                Log.info('Arquivo zip criado com sucesso!');
                var result = Compression.unzip(fileDir.nativePath, zippedFile.nativePath, true);
                if (result == 'success') {
                    Log.info("Arquivo zip descompactado com sucesso");
                    if (!Ti.Filesystem.getFile(fileDir.nativePath, 'content.txt').exists()) {
                        Log.info('FAIL: The unzipped content.txt does not exist!');
                    } else {
                        var max_pages_aux = data["max_pages"];
                        var stringObj = Ti.Filesystem.getFile(fileDir.nativePath, 'content.txt').read().text;
                        data = JSON.parse(stringObj);
                        data["max_pages"] = max_pages_aux;
                    }
                } else {
                    Log.info("Não foi possível descompactar a página");
                }

            }

            // dispose of file handles
            zippedFile = null;
            fileDir = null;
        } catch (error) {
            logError('Erro ao criar a página', error);
            Log.info('Download Sync Callback: ' + error);
            loading.close();
            // closeWindow(loading);
            syncError = true;
        }

        var query = '';
        var queryArr = [];
        for (var i in data) {
            if (typeof data[i] != "undefined") {
                execBatchQuery(data[i]['Query'], queryArr);
                if (data[i]['has_att']) {
                    Log.info("Existe anexo");
                    attData.push({
                        rowid_: data[i]['rowid_'],
                        table: data[i]['table'],
                        bc: data[i]['BC'],
                        att_field: data[i]['att_field'],
                        file_name: data[i]['file_name'],
                        att_field_col_name: data[i]['att_field_col_name']
                    });
                }
            }
        }

        var isTransOK = dbOps.batchTransaction(queryArr);

        if (!isTransOK) {
            Ti.API.info('=== trans is not ok!');
            // var isBatchOK = dbOps.batchUpdate(queryArr);
            syncError = true;
        }

        if ( !! syncError) {
            // closeWindow(loading);
            loading.close();
            var alertCustom = util.createAlertCustom({
                message : L('sync_error_adm', 'Houve um erro na sincronização. Contacte o administrador do sistema. ') + 'SyncDownload.',
                msgIsFormat : true
            });

            alertCustom.open();
            // alert('Houve um erro na sincronização. Contacte o administrador do sistema. SyncDownload');
            return false;
        }

        // Verifica o número de páginas
        if (typeof data["max_pages"] != "undefined") {
            max_pages = data["max_pages"];
            Ti.API.info("max_pages = " + max_pages);
            // loading.setSteps(originalStepsNumber + 2 * max_pages);
        }

        // Realiza a chamada página por página
        if (page <= max_pages) {
            SyncDownload(page);
            page++;
            return;
        }

        // Atualiza caminho dos campos de anexo
        Ti.API.info('Atualizando caminhos dos registros com anexo.');
        var fileUpdArr = [];
        var fileDir;
        if (osname == 'android')
            fileDir = Ti.Filesystem.getFile(Ti.Filesystem.externalStorageDirectory, '.emob/downloaded_images'); // Cria uma pasta oculta para evitar que usuário apaguem
        else
            fileDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'downloaded_images');

        if (!fileDir.exists()) {
            fileDir.createDirectory();
        }

        Ti.API.info("fileDir = " + fileDir.nativePath);
        for (var j in attData) {
            Ti.API.info("attData[j]['file_name'] = " + attData[j]['file_name']);
            Ti.API.info("attData[j]['file_name'] replace = " + attData[j]['file_name'].replace("../../attachment/", ''));
            var attFileAux = Ti.Filesystem.getFile(fileDir.nativePath, attData[j]['file_name'].replace("../../attachment/", ''));

            var query = "UPDATE " + attData[j]['table'] + " SET " + attData[j]['att_field_col_name'] + "='" + attFileAux.nativePath + "' WHERE rowid='" + attData[j]['rowid_'] + "'";
            if (osname == 'iphone' || osname == 'ipad') {
                query = "UPDATE " + attData[j]['table'] + " SET " + attData[j]['att_field_col_name'] + "= 'downloaded_images/" + attData[j]['file_name'].replace("../../attachment/", '') + "' WHERE rowid='" + attData[j]['rowid_'] + "'";
            }
            execBatchQuery(query, fileUpdArr);

            attFileAux = null;
        }
        fileDir = null;

        Ti.API.info("fileUpdArr length = " + fileUpdArr.length);
        if (fileUpdArr.length > 0) {
            if (!dbOps.batchTransaction(fileUpdArr)) {
                Ti.API.info('=== error in attachment update query!');
                syncError = true;
            }
        }

        attDataUniq = _.uniq(attData, function (item, key, file_name) {
            return item.file_name;
        });

        Ti.API.info("attData length = " + attData.length);
        Ti.API.info("attDataUniq length = " + attDataUniq.length);

        DownloadAtt();
    };

    SyncDownloadAtv = function () {
        Ti.API.info('######### SyncDownloadAtv');

        var sync_date = dbOps.select("SELECT last_upd FROM b_config WHERE config = 'firstInLogin'")[0].last_upd;
        Ti.API.info("A última data de sincronização foi " + sync_date);

        visitaIdArray = dbOps.select("SELECT rowid_ FROM b_visita WHERE status = 'Pendente' AND (last_upd > '" + sync_date + "' OR transf_motivo <> '')");
        Ti.API.info("Buscando atividades para " + visitaIdArray.length + " visitas");

        if (visitaIdArray.length > 0) {
            var method = 'getAtv';
            var params = [];
            for (var i = 0; i < 10; i++) {
                if (visitaIdArray.length > 0)
                    params.push(visitaIdArray.shift().rowid_);
            }
            GetWS(method, params, SyncDownloadAtvCallback, syncronizationId);
        } else {
            DownloadAtt();
        }
    }

    SyncDownloadAtvCallback = function (data) {
        Ti.API.info('######### SyncDownloadAtvCallback');
        if (data['return'] == 'error') {
            loading.close();
            // closeWindow(loading);
            util.logError('Erro no callback de SyncDownload', data['msg']);
            util.createAlertCustom(data['msg']);
            return false;
        }

        var query = '';
        var queryArr = [];
        for (var i in data) {
            execBatchQuery(data[i]['Query'], queryArr, true);
        }

        var isTransOK = dbOps.batchTransaction(queryArr);

        if (!isTransOK) {
            Ti.API.info('=== trans is not ok!');
            // var isBatchOK = dbOps.batchUpdate(queryArr);
            syncError = true;
        }

        if ( !! syncError) {
            // closeWindow(loading);
            loading.close();
            var alertCustom = util.createAlertCustom({
                message : L('sync_error_adm', 'Houve um erro na sincronização. Contacte o administrador do sistema. ') + 'SyncDownload.',
                msgIsFormat : true
            });

            alertCustom.open();
            // alert('Houve um erro na sincronização. Contacte o administrador do sistema. SyncDownload');
            return false;
        }

        if (visitaIdArray.length > 0) {
            var method = 'getAtv';
            var params = [];
            for (var i = 0; i < 10; i++) {
                if (visitaIdArray.length > 0)
                    params.push(visitaIdArray.shift().rowid_);
            }
            GetWS(method, params, SyncDownloadAtvCallback, syncronizationId);
        } else {
            DownloadAtt();
        }
    }

    DownloadAtt = function () {

        // loading.increment('Baixando anexos');
        loading.increment('sync_download_attachment');
        loading.setProgressProps({
            detail: 'sync_waiting_server'
            // detail: 'Aguardando servidor'
        });

        if (attDataUniq.length > 0) {
            Ti.API.info('######### DownloadAtt');
            var method = 'DownloadAtt';
            attElem = attDataUniq.shift();
            var params = {
                data: {
                    bc: attElem['bc'],
                    rowid_: attElem['rowid_'],
                    att_field: attElem['att_field']
                }
            };
            GetWS(method, params, DownloadAttCallback, syncronizationId);
        } else {
            SyncUpload();
        }
    };

    DownloadAttCallback = function (data) {

        loading.setProgressProps({
            detail : 'sync_attachment'
            // detail: 'Criando anexos'
        });

        if (data['base64'])
            getAttachmentsFiles(data['base64'], attElem['rowid_'], attElem['table'], attElem['att_field'], attElem['file_name']);

        if (attDataUniq.length > 0) {
            Ti.API.info('######### DownloadAtt');
            var method = 'DownloadAtt';
            attElem = attDataUniq.shift();
            var params = {
                data: {
                    bc: attElem['bc'],
                    rowid_: attElem['rowid_'],
                    att_field: attElem['att_field']
                }
            };
            GetWS(method, params, DownloadAttCallback, syncronizationId);
        } else {
            SyncUpload();
        }
    };

    execQuery = function (query) {

        Ti.API.info('######### ExecQuery');
        try {
            if (query) {
                query = decodeURIComponent(query);
                query = query.replace('ROWID', 'ROWID_');
                query = query.replace('rowid', 'rowid_');


                // Ti.API.info('query: ' + query);

                var isInsert = (query.toUpperCase().indexOf('INSERT') === 0) ? true : false;
                var isUpdate = (query.toUpperCase().indexOf('UPDATE') === 0) ? true : false;

                if (isInsert) {

                    query = query.replace(' ', ' OR REPLACE ');

                    // var table = query.match(/(b_|t_)[^\s]*/i)[0];

                    // var cols_n_vals = query.match(/\([^\)]*/gm);

                    // cols_list = cols_n_vals[0].replace(/\(/gm, '');
                    // cols = cols_list.match(/[^\s\,][^\s\,]*/gm);

                    // var rowid_index;
                    // for (rowid_index = 0; rowid_index < cols.length; rowid_index++) {
                    //  if (cols[rowid_index].toUpperCase() == 'ROWID_') {
                    //      break;
                    //  }
                    // }

                    // vals_list = cols_n_vals[1].replace(/\(/gm, '');
                    // vals_list = vals_list.replace(/\'\'/gm, '\'?\'');

                    // vals = vals_list.match(/[^\'\,\s][^\']*/gm);
                    // var rowid = vals[rowid_index];

                    // var verify_query = 'SELECT rowid_ FROM ' + table + ' WHERE rowid_ = "' + rowid + '"';
                    // var verify_result = dbOps.select(verify_query);

                    // if (verify_result.length > 0) {
                    //  infowarn('Sync - execQuery: Registro já existente!');
                    // } else {
                    dbOps.update({
                        query: query,
                        dontSaveForSync: true
                    });
                    // }

                } else if (isUpdate) {

                    query = query.replace(' ', ' OR IGNORE ');

                    var lastUpd = query.match(/LAST_UPD = \'[^\']*/i);
                    lastUpd = lastUpd[0].replace(/LAST_UPD = \'/i, '');

                    dbOps.update({
                        query: query + " AND LAST_UPD < '" + lastUpd + "'",
                        dontSaveForSync: true
                    });
                } else {

                    // query = query.replace(' ', ' OR IGNORE ');

                    dbOps.update({
                        query: query,
                        dontSaveForSync: true
                    });
                }


            }
        } catch (err) {
            Ti.API.info('execQuery error: ' + err);

            if (typeof err == 'string') {
                Ti.API.info("err = string");
                util.logError('Erro na inserção/atualização de registros', err);
                Ti.API.info("err = string");
            } else if (typeof err == 'object') {
                Ti.API.info("err = object");
                util.logError('Erro na inserção/atualização de registros', '' + err);
                Ti.API.info("err = " + JSON.stringify(err) + " err2 = " + err);
            }
            // Evita o erro quando queries de insert repetidas são executadas
            if (err == 'Error: column ROWID_ is not unique (code 19)') {
                Ti.API.error('ROWID_ not unique error');
                return true;
            }
            // loading.close();
            // closeWindow(loading);
            // syncError = true;
            return false;
        }
        return true;
    };

    execBatchQuery = function (query, arr, ignoreOnInsert) {


        try {
            if (query) {
                query = decodeURIComponent(query);
                query = query.replace('ROWID', 'ROWID_');
                query = query.replace('rowid', 'rowid_');

                Ti.API.info('######### ExecBatchQuery: ' + query);

                // Ti.API.info('query: ' + query);
                var isInsert = (query.toUpperCase().indexOf('INSERT') === 0) ? true : false;
                var isUpdate = (query.toUpperCase().indexOf('UPDATE') === 0) ? true : false;

                if (isInsert) {
                    if (ignoreOnInsert)
                        query = query.replace(' ', ' OR IGNORE ');
                    else
                        query = query.replace(' ', ' OR REPLACE ');

                    // var table = query.match(/(b_|t_)[^\s]*/i)[0];

                    // var cols_n_vals = query.match(/\([^\)]*/gm);

                    // cols_list = cols_n_vals[0].replace(/\(/gm, '');
                    // cols = cols_list.match(/[^\s\,][^\s\,]*/gm);

                    // var rowid_index;
                    // for (rowid_index = 0; rowid_index < cols.length; rowid_index++) {
                    //  if (cols[rowid_index].toUpperCase() == 'ROWID_') {
                    //      break;
                    //  }
                    // }

                    // vals_list = cols_n_vals[1].replace(/\(/gm, '');
                    // vals_list = vals_list.replace(/\'\'/gm, '\'?\'');

                    // vals = vals_list.match(/[^\'\,\s][^\']*/gm);
                    // var rowid = vals[rowid_index];

                    // var verify_query = 'SELECT rowid_ FROM ' + table + ' WHERE rowid_ = "' + rowid + '"';
                    // var verify_result = dbOps.select(verify_query);

                    // if (verify_result.length > 0) {
                    //  infowarn('Sync - execQuery: Registro já existente!');
                    // } else {

                    // dbOps.update({
                    //  query: query,
                    //  dontSaveForSync: true
                    // });

                    // }

                    arr.push(query);

                } else if (isUpdate) {
                    query = query.replace(' ', ' OR IGNORE ');
                    var lastUpd = query.match(/LAST_UPD = \'[^\']*/i);
                    if (lastUpd && lastUpd.length > 0) {
                        lastUpd = lastUpd[0].replace(/LAST_UPD = \'/i, '');
                        query += " AND LAST_UPD <= '" + lastUpd + "'"; // Para evitar que registros alterados no mobile sejam modificados por alterações passadas do servidor
                    }

                    // dbOps.update({
                    //  query: query + " AND LAST_UPD < '" + lastUpd + "'",
                    //  dontSaveForSync: true
                    // });

                    arr.push(query);
                } else {
                    query = query.replace(' ', ' OR IGNORE ');
                    // dbOps.update({
                    //  query: query,
                    //  dontSaveForSync: true
                    // });
                    arr.push(query);

                }
            }
        } catch (err) {
            Ti.API.info('execQuery error: ' + err);

            if (typeof err == 'string') {
                Ti.API.info("err = string");
                util.logError('Erro na inserção/atualização de registros', err);
                Ti.API.info("err = string");
            } else if (typeof err == 'object') {
                Ti.API.info("err = object");
                util.logError('Erro na inserção/atualização de registros', '' + err);
                Ti.API.info("err = " + JSON.stringify(err) + " err2 = " + err);
            }
            // Evita o erro quando queries de insert repetidas são executadas
            if (err == 'Error: column ROWID_ is not unique (code 19)') {
                Ti.API.error('ROWID_ not unique error');
                return true;
            }
            // loading.close();
            // closeWindow(loading);
            // syncError = true;
            return false;
        }
        return true;
    };

    getAttachmentsFiles = function (base64string, rowid, table, field, fileName) {
        try {
            var fileDir;
            if (osname == 'android')
                fileDir = Ti.Filesystem.getFile(Ti.Filesystem.externalStorageDirectory, '.emob/downloaded_images'); // Cria uma pasta oculta para evitar que usuário apaguem
            else
                fileDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'downloaded_images');

            if (!fileDir.exists()) {
                fileDir.createDirectory();
            }

            // var newFileName = guid() + fileName.match(/\.(?=[^.]*$)(.*)/)[0];
            // var oldFilePath;

            // base64string = base64string.replace(/\\/gm, '');

            // .resolve() provides the resolved native path for the directory.
            var attFile = Ti.Filesystem.getFile(fileDir.nativePath, fileName.replace("../../attachment/", ''));
            Ti.API.info("attFile path is: " + attFile.resolve());
            if (attFile.write(Ti.Utils.base64decode(base64string)) === false && osname == 'android') {
                util.logError('Erro na criação de anexo', 'Não foi possível criar o anexo - Write Error');
                Ti.API.error('Não foi possível criar a foto - Write Error');
            } else if ((osname == 'iphone' || osname == 'ipad') && !attFile.isFile()) {
                util.logError('Erro na criação de anexo', 'Não foi possível criar o anexo - Not a File');
                Ti.API.error('Não foi possível criar a foto - Not a File');
            } else {
                Ti.API.info('Anexo criado com sucesso!');
                // oldFilePath = dbOps.select("SELECT " + field + " FROM " + table + " WHERE rowid_='" + rowid + "'");

                // Deleta a foto antiga
                // var oldFile = null;
                // if (oldFilePath.length > 0)
                //  oldFile = Ti.Filesystem.getFile(oldFilePath[0][field]);

                // if (oldFile) {
                //  if (oldFile.deleteFile()) {
                //      Ti.API.info('Arquivo deletado com sucesso');
                //  } else {
                //      Ti.API.info('O arquivo não pode ser deletado');
                //  }
                // } else {
                //  Ti.API.info('Não existe arquivo antigo.');
                // }

                // Atualiza campo de anexo
                // // dbOps.update({
                // //   query: "UPDATE " + table + " SET " + field + "='" + attFile.nativePath + "' WHERE rowid_='" + rowid + "'",
                // //   dontSaveForSync: true
                // // });
                // Ti.API.info('Caminho do anexo atualizado.');
            }

            // dispose of file handles
            attFile = null;
            fileDir = null;
        } catch (error) {
            util.logError('Erro ao criar anexo', error);
            Ti.API.info('getAttachmentsFiles: ' + error);
            loading.close();
            // closeWindow(loading);
            syncError = true;
        }
    };

    this.StartSync = function () {
        if (Titanium.Network.networkType == Titanium.Network.NETWORK_NONE) {
            util.logError('Erro ao iniciar sincronização', 'Sem conexão com a internet');
            // copyDb();
            var alertCustom = util.createAlertCustom({
                message : 'no_internet'
                // message : 'Sem conexão com a internet.'
            });

            alertCustom.open();
            // alert('Sem conexão com a internet.');
        } else {
            loading.open();
            SyncSQL();
            // SyncDownload();
            // copyDb();
        }
    };

    this.HasError = function () {
        if (syncError === true) {
            // closeWindow(loading);
            return true;
        }
        return false;
    };
    this.GetError = function () {
        return errorStr;
    };

    // Deleta todos registros relacionados a visitas que já tiverem sido realizadas

    function deleteData() {
        Ti.API.info("Deletando registros já utilizados");

        // Arquivos
        var delQuery = "SELECT t1.rowid_, t1.url FROM b_attachment t1 " +
            "JOIN b_evt_act t2 ON t1.act_id = t2.rowid_ " +
            "JOIN b_visita t3 ON t2.visita_id = t3.rowid_ " +
            "WHERE t3.status = 'Realizada' OR t3.status = 'Não Realizada' OR t3.status = 'Cancelada'";
        if (cargo == 'promotor') {
            delQuery += " OR t3.responsible_id <> '" + user_id + "'";
        }
        var attachResult = dbOps.select(delQuery);
        for (var i in attachResult) {
            var attachment;
            if (osname == 'android') {
                if (Ti.Filesystem.isExternalStoragePresent) {
                    var fileDir = Ti.Filesystem.getFile(Ti.Filesystem.externalStorageDirectory, '.camera');
                    if (fileDir.exists()) {
                        attachment = Ti.Filesystem.getFile(fileDir.nativePath, attachResult[i].url);
                    } else {
                        attachment = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, attachResult[i].url);
                    }
                } else {
                    attachment = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, attachResult[i].url);
                }
            } else {
                attachment = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, attachResult[i].url);
            }
            Ti.API.info("URL = " + attachment.nativePath);
            if (attachment.exists()) {
                if (attachment.deleteFile()) {
                    Ti.API.info("Anexo deletado com sucesso.");
                } else {
                    Ti.API.info("Não foi possível deletar o arquivo.");
                }
            } else {
                Ti.API.info("Anexo não existe mais.");
            }
        }

        // Anexos
        if (cargo == 'supervisor') {
            delQuery = "DELETE FROM b_attachment WHERE rowid_ IN (SELECT t1.rowid_ FROM b_attachment t1 " +
                "JOIN b_evt_act t2 ON t1.act_id = t2.rowid_ " +
                "JOIN b_visita t3 ON t2.visita_id = t3.rowid_ " +
                "WHERE t3.status = 'Realizada' OR t3.status = 'Não Realizada' OR t3.status = 'Cancelada')";
        } else {
            delQuery = "DELETE FROM b_attachment WHERE rowid_ IN (SELECT t1.rowid_ FROM b_attachment t1 " +
                "JOIN b_evt_act t2 ON t1.act_id = t2.rowid_ " +
                "JOIN b_visita t3 ON t2.visita_id = t3.rowid_ " +
                "WHERE t3.status = 'Realizada' OR t3.status = 'Não Realizada' OR t3.status = 'Cancelada' OR t3.responsible_id <> '" + user_id + "')";
        }
        dbOps.del({
            query: delQuery
        });
        Ti.API.info("Registros já utilizados de b_attachment deletados");

        // Atividades
        if (cargo == 'supervisor') {
            delQuery = "DELETE FROM b_evt_act WHERE rowid_ IN (SELECT t1.rowid_ FROM b_evt_act t1 " +
                "JOIN b_visita t2 ON t1.visita_id = t2.rowid_ " +
                "WHERE t2.status = 'Realizada' OR t2.status = 'Não Realizada' OR t2.status = 'Cancelada')";
        } else {
            delQuery = "DELETE FROM b_evt_act WHERE rowid_ IN (SELECT t1.rowid_ FROM b_evt_act t1 " +
                "JOIN b_visita t2 ON t1.visita_id = t2.rowid_ " +
                "WHERE t2.status = 'Realizada' OR t2.status = 'Não Realizada' OR t2.status = 'Cancelada' OR t2.responsible_id <> '" + user_id + "')";
        }
        dbOps.del({
            query: delQuery
        });
        Ti.API.info("Registros já utilizados de b_evt_act deletados");

        // Logs de Tempos
        if (cargo == 'supervisor')
            delQuery = "DELETE FROM b_tempo_tela WHERE rowid_ IN (SELECT t1.rowid_ FROM b_tempo_tela t1 JOIN b_visita t2 ON t1.visita_id = t2.rowid_ AND (t2.status = 'Realizada' OR t2.status = 'Não Realizada' OR t2.status = 'Cancelada'))";
        else
            delQuery = "DELETE FROM b_tempo_tela WHERE rowid_ IN (SELECT t1.rowid_ FROM b_tempo_tela t1 JOIN b_visita t2 ON t1.visita_id = t2.rowid_ AND (t2.status = 'Realizada' OR t2.status = 'Não Realizada' OR t2.status = 'Cancelada' OR t2.responsible_id <> '" + user_id + "'))";
        dbOps.del({
            query: delQuery
        });
        Ti.API.info("Registros de logs de tempos deletados");

        // Visitas
        delQuery = "DELETE FROM b_visita WHERE status = 'Realizada' OR status = 'Não Realizada' OR status = 'Cancelada'";
        if (cargo == 'promotor') {
            delQuery += " OR responsible_id <> '" + user_id + "'";
        }
        dbOps.del({
            query: delQuery
        });
        Ti.API.info("Registros já utilizados de b_visita deletados");
    }

    // Copia o banco de dados para o SDCARD para debug

    function copyDb() {
        if (DEBUG) {
            if (osname == 'android') {
                var dbFile = Ti.Filesystem.getFile('file:///data/data/com.bexpert.inova.hyper.trade/databases/data');
                if (dbFile.exists()) {
                    if (dbFile.copy(Ti.Filesystem.externalStorageDirectory + '/data.sqlite')) {
                        Ti.API.info('Database copiado para o sdcard');
                    } else {
                        Ti.API.info('Não foi possível copiar o database');
                    }
                } else {
                    Ti.API.info('O arquivo não existe');
                }
            }
        }
    }

    function guid() {
        return Math.floor((1 + Math.random()) * 1000000000).toString(16) + '-' + Math.floor((1 + Math.random()) * 0x1000).toString(16) + '-' + Math.floor((1 + Math.random()) * 0x1000).toString(16) + '-' + Math.floor((1 + Math.random()) * 0x1000).toString(16) + '-' + Math.floor((1 + Math.random()) * 100000000000000).toString(16);
    }

}